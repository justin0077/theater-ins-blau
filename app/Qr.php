<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qr extends Model
{
  protected $fillable = [
    'page', 'page_id',
  ];

  public $timestamps = false;
}
