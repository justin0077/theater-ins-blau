<?php

namespace App\Http\Controllers;

use App\Qr;
use Illuminate\Http\Request;

class QrController extends Controller
{
  public function api() {

    $allQr = Qr::all();
    return $allQr;

  }

  public function show($id){
    $qr = Qr::find($id);

    //redirect naar de pagina met het ingestelde id van de QR
    return redirect()->route('pages', [$qr->page_id]);
  }
}
