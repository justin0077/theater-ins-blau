<?php

namespace App\Http\Controllers;

use App\Picture;
use App\Page;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Image;

class PhotoController extends Controller
{

    protected $pictures;

    public function __construct(Picture $pictures)
    {
      $this->picture = $pictures;
    }

    public function index() {

      $picture = Picture::paginate(10);
      return view('fotos.photolist')->with('pictures', $picture, compact('pictures'));

    }

    public function upload(Picture $pictures)
    {
      $pages = Page::all();
      $pages= Page::pluck('pagename', 'id')->toArray();
        return view('fotos.upload', compact('pictures','pages',$pages));
    }

    public function savePicture(Request $request)
    {

        $picture = new Picture;

        if($request->hasFile('pic')) {
          $file = $request->file('pic');
          $extension = $file->getClientOriginalExtension();
          $filename = time() . '.' . $extension;
          $file->move('images/', $filename);
          $picture->pic = $filename;
        }

        $picture->name = $request->get('name');
        $picture->status = $request->get('photo');

        $picture->save();

         return Redirect::to('fotos/photolist')->with('success', 'Nieuwe foto toegevoegd!');
    }

    public function edit($id)
    {
      $pictures = $this->picture->findOrFail($id);
      $pages = Page::all();
      $pages= Page::pluck('pagename', 'id')->toArray();

      return view('fotos.upload', compact('pictures', 'pages',$pages));
    }

    public function update(Request $request, $id)
    {
      $picture = $this->picture->findOrFail($id);

      $picture->name = $request->input('name');
      $picture->status = $request->input('photo');
      $picture->save();

      return Redirect::to('fotos/photolist')->with('success', 'Foto is gewijzigd!');
    }

    public function confirm($id)
    {
        $pictures = $this->picture->findOrFail($id);

        return view('fotos.confirm', compact('pictures'));
    }

    public function destroy($id)
    {
        $picture = $this->picture->findOrFail($id);

        $picture->delete();

        return Redirect::to('/fotos/photolist')->with('success', 'Foto is verwijderd!');

    }





}
