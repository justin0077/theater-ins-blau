<?php

namespace App\Http\Controllers;

use App\Page;
use App\Picture;
use Illuminate\Http\Request;

class frontController extends Controller
{
  public function index()
  {
      $pages = Page::all();
      $pictures = Picture::all();
      return view('welcome', compact('pages', 'pictures'));
  }
}
