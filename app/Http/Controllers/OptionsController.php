<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Qr;
use App\Page;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class OptionsController extends Controller
{

    protected $qrs;
    protected $pages;

    public function __construct(QR $qrs, Page $pages)
    {
      $this->qr = $qrs;
      $this->pages = $pages;
    }

    public function index() {
      $qr = Qr::paginate(4);
      return view('instellingen')->with('qrs', $qr, compact('qrs'));
    }

    public function edit($id)
    {
      $qr = Qr::find($id);
      $pages = Page::pluck('pagename', 'id');
      return view('instellingen.edit', compact('qr', 'pages'));
    }

    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'page' => 'required'
      ]);

      // stel de pagina in als redirect voor wanneer de QR gescant wordt.
      $qr = Qr::find($id);
      $qr->page = $request->input('page');
      $qr->page_id = $request->input('page-id');
      $qr->save();

      return Redirect::to('instellingen')->with('success', 'Naam is gewijzigd!');
    }




}
