<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Picture;
use App\Page;
use Redirect;
use DB;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $pictures;
     protected $pages;

     public function __construct(Picture $pictures, Page $pages)
     {
       $this->picture = $pictures;
       $this->pages = $pages;
     }


    public function index()
    {
      $pages = Page::all();
      return view('pages.index')->with('pages',$pages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pictures = Picture::all();

        return view('pages.create')->with('pictures', $pictures);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'title' => 'required',
        'content' => 'required'
      ]);

      // aanmaken van een pagina
      $page = new Page;
      $page->pagename = $request->input('title');
      $page->content = $request->input('content');

      // voor de cover wordt de foto opgeslagen op de server en de link de naar het bestand wordt opgeslagen in de database
      if($request->hasFile('cover')) {
        $file = $request->file('cover');
        $extension = $file->getClientOriginalExtension();
        $filename = time() . '.' . $extension;
        $file->move('images/', $filename);
        $page->cover = $filename;
      }

      // sla de pagina op in de database
      $page->save();

      return redirect('/pages')->with('success', 'De pagina is aangemaakt!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = Page::findOrFail($id);

        // pak alle foto's de horen bij de pagina van het ID om deze mee te geven aan de pagina.
        $pictures = DB::select('select * from pictures where status = :id', ['id' => $id]);

        return view('pages.show', compact('page', 'pictures'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::find($id);
        return view('pages.edit')->with('page', $page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
        'title' => 'required',
        'content' => 'required'
      ]);

      // pagina aanmaken
      $page = Page::find($id);
      $page->pagename = $request->input('title');
      $page->content = $request->input('content');
      $page->save();

      return redirect('/pages')->with('success', 'De pagina is gewijzigd!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         $page = $this->pages->findOrFail($id);

         $page->delete();

         return Redirect::to('pages')->with('success', 'Pagina is succesvol verwijderd!');

     }
}
