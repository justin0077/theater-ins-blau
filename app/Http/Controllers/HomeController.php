<?php

namespace App\Http\Controllers;

use App\Page;
use App\Qr;
use App\Picture;
use App\User;
use Illuminate\Http\Request;
use DB;
use Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $pages;

    public function __construct(Page $pages)
    {
        $this->middleware('auth');

        $this->pages = $pages;
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pages = $this->pages->all();
        $pictures = Picture::paginate(10);
        $qrs = Qr::all();
        return view('home', compact('pages', 'pictures', 'qrs'));
    }


}
