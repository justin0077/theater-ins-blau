<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Image;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function index() {
      return view('user/index', array('user' => Auth::user() ));
    }

    public function update_avatar(Request $request) {

          // Handle the user upload of avatar
          if($request->hasFile('foto')){
              $avatar = $request->file('foto');
              $filename = time() . '.' . $avatar->getClientOriginalExtension();
              Image::make($avatar)->resize(300, 300)->save( public_path('/images/profile/' . $filename ) );
              $user = Auth::user();
              $user->foto = $filename;
              $user->save();
          }
          return redirect('user')->with('success','Foto succesvol geüpdatet');
      }
}
