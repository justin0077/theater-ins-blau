<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index');

Auth::routes();

Route::middleware(['auth'])->group(function () {

  Route::get('/home', 'HomeController@index')->name('home');
  Route::delete('/home/{id}/delete', 'PageController@destroy');

  Route::resource('pages', 'PageController')->except('show');

  Route::get('/fotos/photolist', 'PhotoController@index');
  Route::get('/fotos/upload', 'PhotoController@upload');
  Route::post('add', 'PhotoController@savePicture');
  Route::get('fotos/pic/{id}', 'PhotoController@showPicture');

  Route::get('fotos/{picture}/confirm', ['as' =>'fotos.confirm', 'uses' => 'PhotoController@confirm']);
  Route::delete('fotos/{picture}/delete', ['as' => 'fotos.destroy', 'uses' => 'PhotoController@destroy']);

  Route::resource('fotos', 'PhotoController');

  Route::get('instellingen', 'OptionsController@index');
  Route::get('/instellingen/edit/{id}', ['as' => 'instellingen.edit', 'uses' => 'OptionsController@edit']);

  Route::get('user', 'UserController@index');
  Route::post('user', 'UserController@update_avatar');

  Route::resource('instellingen', 'OptionsController');

});

Route::get('/pages/{id}', ['as' => 'pages', 'uses' =>'PageController@show']);
Route::get('/qr/api', 'QrController@api');
Route::get('/qr/{id}', 'QrController@show');
