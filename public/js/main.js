console.log("main aangeroepen");

// Aanroepen van verwijder modal
$(function() {
    $('#deleteModal').on("show.bs.modal", function(e) {
        $("#deleteModalLabel").html($(e.relatedTarget).data('name'));
        $('#confirmDelete').attr('action', "/home/" + $(e.relatedTarget).data('id') + "/delete");
    });
});
