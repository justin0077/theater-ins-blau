<?php

use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('pages')->insert([
          [
              'pagename' => 'Pagina1'
          ],
          [
              'pagename' => 'Pagina2'
          ],
          [
              'pagename' => 'Pagina3'
          ],
          [
              'pagename' => 'Pagina4'
          ]
        ]);
    }
}
