<?php

use Illuminate\Database\Seeder;

class qrSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('qrs')->insert([
          [
              'page' => 'Pagina1',
              'page_id' => '1'
          ],
          [
              'page' => 'Pagina2',
              'page_id' => '2'
          ],
          [
              'page' => 'Pagina3',
              'page_id' => '4'
          ],
          [
              'page' => 'Pagina4',
              'page_id' => '4'
          ]
        ]);
    }
}
