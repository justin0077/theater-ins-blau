@extends('layouts.backend')

@section('header')
  <h1 class="header-title">Pagina aanmaken</h1>
@endsection

@section('content')
<div class="container">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        Pagina aanmaken
      </div>
      <div class="card-body">
        {!! Form::open(['action' => 'PageController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
          <div class="form-group">
              {{Form::label('title', 'Title')}}
              {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
          </div>
          <div class="form-group">
              {{Form::label('content', 'Content')}}
              {{Form::textarea('content', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Content'])}}
          </div>
          <p>Cover toevoegen</p>
          <div class="form-group addimage">
              {!! Form::file('cover') !!}
            </a>
          </div>
          {{Form::submit('Pagina aanmaken', ['class'=>'btn btn-primary'])}}
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>




<!-- Modal -->
<div class="modal fade" id="picturemodal" tabindex="-1" role="dialog" aria-labelledby="picturemodallabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">foto toevoegen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Welke foto wil je toevoegen</p>
        @foreach($pictures as $picture)
        <img src="{{ asset('images/' . $picture->pic) }}" alt="foto" class="image" style="width: 150px; height: 120px;">
        @endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluit</button>
        <form id="confirmAdd" action="/home/" method="POST">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
              <button class="btn btn-info">Toevoegen</button>
        </form>

      </div>
    </div>
  </div>
</div>
@endsection
