@extends('layouts.app')

@section('content')
<div class="container">
  @auth
    <a class="btn btn-info" href="/pages/{{$page->id}}/edit" role="button">
        Pagina aanpassen
    </a>
  @endauth
<div class="col-xs-1" align="center">
  <h3 class="pb-3 mb-4 font-italic border-bottom">{{$page->pagename}}</h3>
  <br>
  <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
          <div class="col-md-6 px-0">
  {!!$page->content!!}

          </div>
          <div class="col-md-12 px-0">
            @foreach ($pictures as $picture)
                <img src="{{ asset('images/' . $picture->pic) }}" alt="foto" class="image" style="width: 250px; height: 150px;">
            @endforeach
          </div>
        </div>


</div>
</div>
@endsection
