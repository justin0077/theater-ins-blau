@extends('layouts.backend')

@section('header')
  <h1 class="header-title">Pagina aanmaken</h1>
@endsection

@section('content')
<div class="container">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        Pagina aanpassen
      </div>
      <div class="card-body">
        <a class="btn btn-secondary" href="/pages/" role="button">Terug zonder op te slaan</a>
        <br><br>

        {!! Form::open(['action' => ['PageController@update', $page->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
          <div class="form-group">
              {{Form::label('title', 'Title')}}
              {{Form::text('title', $page->pagename, ['class' => 'form-control', 'placeholder' => 'Title'])}}
          </div>
          <div class="form-group">
              {{Form::label('content', 'Content')}}
              {{Form::textarea('content', $page->content, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Content'])}}
          </div>
          <div class="form-group">
              {{Form::label('cover', 'Cover')}}
              {{Form::file('cover_image')}}
          </div>
          {{Form::submit('wijzigingen opslaan', ['class'=>'btn btn-primary'])}}
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection
