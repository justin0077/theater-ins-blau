@extends('layouts.backend')

@section('header')
  <h1 class="header-title">Pagina's</h1>
@endsection

@section('content')
<!-- Hier komt de content van de tabs -->
<div class="container">
  <div class="col-md-12 full-width">
    <div class="card">
      <div class="card-header">
        Pagina's
      </div>
      @if(Session::has('success'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      {{Session::get('success')}}
      </div>
      @endif

      <div class="card-body">
        <p>Hier kan je de pagina's wijzigen of verwijderen</p>

        <a class="btn btn-info" href="/pages/create" role="button">
          <i class="material-icons foto">control_point</i>
            Nieuw
        </a>
        <br>
        <br>

        @if($pages->isEmpty())
          <h4>Geen pagina's</h4>
        @else
        <div class="table-responsive-sm">
          <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th class="col-md-2">Pagina</th>
                <th>View</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            @foreach($pages as $page)
              <tbody class="pages">
                <tr>
                  <th scope="row">{!! $page->id !!}</th>
                  <td>{!! $page->pagename !!}</td>
                  <td>
                    <a href="/pages/{{$page->id}}">
                      <i class="material-icons collections">pageview</i>
                    </a>
                  </td>
                  <td>
                    <a href="/pages/{{$page->id}}/edit">
                        <i class="material-icons update">edit</i>
                    </a>
                  </td>
                  <td>
                    <a href="#" data-toggle="modal" data-target="#deleteModal" data-id="{{ $page->id }}" data-name="{{ $page->pagename }}">
                      <i class="material-icons delete">delete</i>
                     </a>
                  </td>
                </tr>
              @endforeach
              </tbody>
          </table>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Pagina verwijderen</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Weet je zeker dat je <span style="font-weight: bold;" id="deleteModalLabel">{{ $page->pagename }}</span> wilt verwijderen.<br>Je kunt deze actie niet ongedaan maken!</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluit</button>
              <form id="confirmDelete" action="/pages/{{ $page->id }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button class="btn btn-danger">Verwijder</button>
              </form>

            </div>
          </div>
        </div>
      </div>
      @endif


@endsection
