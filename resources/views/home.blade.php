@extends('layouts.backend')

@section('header')
  <h1 class="header-title">Dashboard</h1>
@endsection

@section('content')
            <!-- Hier komt de content van de tabs -->
            <div class="container bottom-80">
              <div class="col-md-12">
                <div class="card">
                  @if(Session::has('success'))
                  <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  {{Session::get('success')}}
                  </div>
                  @endif
                    <div class="card-body">
                        <p class="loginwelcome">Welkom {{ Auth::user()->name }}!</p>
                        <hr>
                        <div class="row">
                          <div class="col">
                            <div class="card">
                              <div class="card-header">
                                Recente Pagina's
                              </div>
                              <div class="card-body">
                                <p>Hier kan je de pagina's wijzigen of verwijderen</p>

                                @if($pages->isEmpty())
                                <hr>
                                  <h4>Geen pagina's</h4>
                                @else
                                <table class="table table-striped table-hover">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th class="col-md-2">Pagina</th>
                                      <th>View</th>
                                    </tr>
                                  </thead>
                                    <tbody`class="pages">
                                      @foreach($pages as $page)
                                      <tr>
                                        <th scope="row">1</th>
                                        <td>{!! $page->pagename !!}</td>
                                        <td>
                                          <a href="{{ route('pages', ['id' => $page->id]) }}">
                                            <i class="material-icons collections">pageview</i>
                                          </a>
                                        </td>
                                      </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                @endif
                              </div>
                            </div>
                          </div>
                          <div class="col">
                            <div class="card">
                              <div class="card-header">
                                Actieve links
                              </div>
                              <div class="card-body">
                                <table class="table table-striped table-hover">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th class="col-md-2">link</th>
                                      <th>View</th>
                                      <th>Edit</th>
                                    </tr>
                                  </thead>
                                    <tbody`class="pages">
                                    @foreach($qrs as $qr)
                                        <tr>
                                          <th scope="row">{{ $qr->id }}</th>
                                          <td>{!! $qr->page !!}</td>
                                          <td>
                                          <a href="{{ route('pages', ['id' => $qr->page_id]) }}">
                                              <i class="material-icons collections">pageview</i>
                                            </a>
                                          </td>
                                          <td>
                                          <a href="{{ route('instellingen.edit', ['id' => $qr->id]) }}">
                                                <i class="material-icons update">edit</i>
                                            </a>
                                          </td>
                                        </tr>
                                      @endforeach
                                    </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                        <hr>
                        <div class="row">
                          <div class="col">
                            <div class="card">
                              <div class="card-header">
                                Recente foto's
                              </div>
                              <div class="card-body">
                              {{ $pictures->links() }}
                                @foreach($pictures as $picture)
                                <img src="{{ asset('images/' . $picture->pic) }}" alt="foto" class="image" style="width: 140px; height: 110px;">
                                @endforeach
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>

@endsection
