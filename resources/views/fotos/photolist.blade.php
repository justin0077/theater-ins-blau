@extends('layouts.backend')

@section('header')
  <h1 class="header-title">Foto's</h1>
@endsection

@section('content')

<div class="container">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        fotolijst
      </div>
      @if(Session::has('success'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      {{Session::get('success')}}
      </div>
      @endif
        <div class="card-body">
          <a class="foto btn btn-info" href="{{ url('/fotos/upload') }}">
            <i class="material-icons foto">control_point</i>
              Nieuw
          </a>
          <div class="table-responsive-sm">
            <table class="table table-striped table-hover">
              <thead>
                <tr>
                  <th>Naam</th>
                  <th>Foto</th>
                  <th>Status</th>
                  <th style="width:10%">View</th>
                  <th style="width:10%">Edit</th>
                  <th style="width:10%">Delete</th>
                </tr>
              </thead>
                @foreach($pictures as $picture)
                <tbody class="pages">
                  <tr>
                    <td scope="row">{{ $picture->name }}</td>
                    <td><img src="{{ asset('images/' . $picture->pic) }}" alt="foto" class="image" style="width: 80px; height: 60px;"></td>
                    <td>{{ $picture->status }}</td>
                    <td>
                      <a href="/pages/{{$picture->status}}">
                        <i class="material-icons collections">pageview</i>
                      </a>
                    </td>
                    <td>
                      <a href="{{ route('fotos.edit', $picture->id) }}">
                          <i class="material-icons update">edit</i>
                      </a>
                    </td>
                    <td>
                      <a href="{{ route('fotos.confirm', $picture->id) }}">
                        <i class="material-icons delete">delete</i>
                       </a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
            </table>
          </div>
          <?php echo $pictures->render(); ?>
        </div>
      </div>
    </div>
  </div>

@endsection
