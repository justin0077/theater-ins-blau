@extends('layouts.backend')

@section('content')
@if($pictures->exists)
    {!! Form::model($pictures, ['route' => ['fotos.update', $pictures->id], 'method' => 'put']) !!}
@else
    {!! Form::open(array('url' => 'add', 'method' => 'post', 'files' => true)) !!}
@endif
    <div class="container">
      <div class="col-md-14">
        <div class="card">
          @if(Session::has('success'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          {{Session::get('success')}}
          </div>
          @endif
          <div class="card-header">
            fotolijst
          </div>
            <div class="card-body">
                  <div>
                      <div class="form-group label-floating has-success" style="width:300px;">
                        <label for="name" class="control-label">Naam:</label>
                          <input type="text" name="name" value="" class="form-control" required />
                      </div>

                      <div class="foto-pagina">
                        <h1>Pagina</h1>
                        <div class="col-md-12">
                        {!! Form::select('photo', array('' => 'Selecteer een pagina') + $pages, ['class' => 'form-control']) !!}
                    </div>
                    </div>
                  </div>

                  <div>
                    @if(!($pictures->exists))
                      {!! Form::label('Kies foto:') !!}
                      {!! Form::file('pic') !!}
                    @endif
                      <div>
                          {!! Form::submit($pictures->exists ? 'Wijzig foto' : 'Voeg toe', ['class' => 'btn btn-primary']) !!}
                      </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>

    </div>

    {{ Form::close() }}

@endsection
