@extends('layouts.backend')

@section('title', 'Delete '.$pictures->title)

@section('content')
    {!! Form::open(['method' => 'delete', 'route' => ['fotos.destroy', $pictures->id]]) !!}
    <div class="container">
      <div class="col-md-12">
        <div class="card">
            <div class="card-body">
              <div class="alert alert-danger alert-fotos">
                  <strong>Warning! </strong>Je staat op het punt deze foto te verwijderen! Weet je zeker dat je door wilt gaan?
              </div>
              <hr>

              {!! Form::submit('Ja, verwijder deze foto!', ['class' => 'btn btn-danger']) !!}

              <a href="{{ url('/fotos/photolist') }}" class="btn btn-success" style="float: right;">
                  <strong>Nee, breng me terug!</strong>
              </a>
            </div>
          </div>
        </div>
      </div>
    {!! Form::close() !!}
@endsection
