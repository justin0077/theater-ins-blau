<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Theater Ins Blau</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/frontend.css') }}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #3498db;
                color: white;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: white;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .scroll {
              animation: bouncing 1s infinite ease-in-out;
              position: absolute;
              bottom: 20px;
              height: 50px;
              cursor: pointer;
            }
            .scroll > i {
              font-size: 48px;
            }
            @keyframes bouncing {
              0% {
                bottom: 0;
              }
              50% {
                bottom: 20px;
              }
              100% {
                bottom: 0;
              }
            }
        </style>
        <script>
          window.smoothScroll = function(target) {
              var scrollContainer = target;
              do {
                scrollContainer = scrollContainer.parentNode;
                if (!scrollContainer) return;
                scrollContainer.scrollTop += 1;
              } while (scrollContainer.scrollTop == 0);

              var targetY = 0;
              do {
                if (target == scrollContainer) break;
                targetY += target.offsetTop;
              } while (target = target.offsetParent);

              scroll = function(c, a, b, i) {
                i++;
                if (i > 30) return;
                c.scrollTop = a + (b - a) / 30 * i;
                setTimeout(function() {
                  scroll(c, a, b, i);
                }, 20);
              }
              scroll(scrollContainer, scrollContainer.scrollTop, targetY, 0);
            }
        </script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Theater Ins Blau
                </div>
            </div>
            <div class="scroll" onclick="smoothScroll(document.getElementById('second'))">
              <i class="material-icons">
              keyboard_arrow_down
              </i>
            </div>
        </div>
        <section id="second">
          <div class="container">
            <h1>Voorstellingen</h1>
            <div class="row">
              @foreach ($pages as $page)
                <div class="col-md-3">
                  <div class="voorstelling">
                    <a href="/pages/{{$page->id}}">
                      <img src="{{ asset('images/' . $page->cover) }}" alt="foto" class="image">
                      <p>{{$page->pagename}}</p>
                      <div class="hvrbox-layer_top">
                        <div class="hvrbox-text">
                          <i class="material-icons">
                            arrow_forward_ios
                          </i></div>
                      </div>
                    </a>
                  </div>
                </div>
              @endforeach

            </div>
          </div>
        </section>
    </body>
</html>
