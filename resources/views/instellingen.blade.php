@extends('layouts.backend')

@section('header')
  <h1 class="header-title">Instellingen</h1>
@endsection

@section('content')

<div class="container">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        instellingen
      </div>
      @if(Session::has('success'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      {{Session::get('success')}}
      </div>
      @endif
        <div class="card-body">
          <p>Hier kan de tekst aangepast worden voor op de schermen.</p>
          <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Scherm tekst</th>
                <th scope="col">Edit</th>
              </tr>
            </thead>
            <tbody>
              @foreach($qrs as $qr)
              <tr>
                <th scope="row">{{ $qr->id }}</th>
                <td>{{ $qr->page }}</td>
                <td>
                  <a href="{{ route('instellingen.edit', $qr->id) }}">
                      <i class="material-icons update">edit</i>
                  </a>
                </td>
              </tr>
              @endforeach
            </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>




@endsection
