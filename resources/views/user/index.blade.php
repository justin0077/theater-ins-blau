@extends('layouts.backend')

@section('header')
  <h1 class="header-title">Account</h1>
@endsection

@section('content')



<div class="container">
  <div class="col-md-12 account">
    <div class="card">
      <div class="card-header">
        Account aanpassen
      </div>
      @if(Session::has('success'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      {{Session::get('success')}}
      </div>
      @endif
      <div class="card-body">

          <form enctype="multipart/form-data" action="/user" method="POST">
            <img class="account" src="/images/profile/{{ Auth::user()->foto }}">
            <p class="name">{{ Auth::user()->name }}</p>
            <hr class="border"></hr>
            <p class="foto">Upload nieuwe foto:</p>
            <p class="upload">
              <input type="file" name="foto">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

              <input type="submit" class="pull-right update-btn btn btn-primary" value="update">
            </p>
        </form>

      </div>
    </div>
  </div>
</div>
{!! Form::close() !!}

@endsection
