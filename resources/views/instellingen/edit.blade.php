@extends('layouts.backend')

@section('header')
  <h1 class="header-title">Tekst wijzigen</h1>
@endsection

@section('content')

<div class="container">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        Pagina aanpassen
      </div>
      <div class="card-body">
        <a class="btn btn-secondary" href="/instellingen" role="button">Terug zonder op te slaan</a>
        <br><br><br>

        {!! Form::model($qr, ['route' => ['instellingen.update', $qr->id], 'method' => 'PUT']) !!}
        <div class="col-md-8">
        <div class="form-group">
            {{Form::label('page', 'pagina')}}
            {{Form::select('page-id', $pages, null, ['class' => 'form-control']) }}
        </div>
          <div class="form-group">
            {{Form::label('page', 'Scherm tekst')}}
            {{Form::text('page', $qr->page, ['class' => 'form-control', 'placeholder' => 'page'])}}
        </div>
      </div>
      {{Form::submit('wijzigingen opslaan', ['class'=>'btn btn-primary'])}}

        {!! Form::close() !!}


      </div>
    </div>
  </div>
</div>


@endsection
